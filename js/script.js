// Задание
// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
//
//     Технические требования:
//
//     Считать с помощью модального окна браузера два числа.
//     Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
//     Вывести в консоль результат выполнения функции.
//
//
//     Не обязательное задание продвинутой сложности:
//
//     После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа,
//     - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


let firstNumber = +prompt('Input first number'),
    secondNumber = +prompt('Input second number'),
    mathOperation = prompt('Input math operation: +, -, *, /');

while (isNaN(firstNumber) || isNaN(secondNumber)|| !firstNumber || !secondNumber){
    alert('Error! You entered not the correct numbers');
    firstNumber = +prompt('Input first number',firstNumber);
    secondNumber = +prompt('Input second number',secondNumber);
};

function calculateNumbers(a,b,oper) {
 let result = 0;
    switch (oper) {
        case '+' :
            result = a + b;
            break;
        case '-' :
            result = a - b;
            break;
        case '*' :
            result = a * b;
            break;
        case '/' :
            result = a / b;
            break;
        default :
            result = 'Wrong math operation';
    }
    return result;
}

console.log(calculateNumbers(firstNumber, secondNumber, mathOperation));

